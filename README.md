# Volunteer app

An app to post and find volunteer opportunities nearby with functionality to sort and filter opportunities as well as get a detailed view.

iOS/API technologies:
- User generated data and sample data for the volunteer opportunities
- Form validation when creating opportunities (+ Data Picker?)
- Map views for opportunities

Sources of complexity:
- Location/sorting by distance
- Creating new postings
- Different filtering options for volunteer opportunities
