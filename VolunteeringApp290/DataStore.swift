import Foundation

class DataStore: ObservableObject {
    @Published var opportunities = Opportunity.previewData
    
    func addOpportunity(_ opportunity: Opportunity) {
      if opportunities.firstIndex(where: { $0.id == opportunity.id }) == nil {
        opportunities.append(opportunity)
      }
    }
    
    func removeOpportunity(_ opportunity: Opportunity) {
      if let index = opportunities.firstIndex(where: { $0.id == opportunity.id }) {
        opportunities.remove(at: index)
      }
    }
}
