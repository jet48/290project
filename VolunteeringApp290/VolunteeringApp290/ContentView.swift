//
//  ContentView.swift
//  VolunteeringApp290
//
//  Created by John Thomas on 3/17/22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        TabContainer()
    }
}
