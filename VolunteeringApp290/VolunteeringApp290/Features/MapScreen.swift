
import SwiftUI
import MapKit
import CoreLocation
import CoreLocationUI

struct MapScreen: View {
    @StateObject var viewModel: MapScreenVM
    @EnvironmentObject var locationManager: LocationManager
    
    var body: some View {
        VStack {
            HStack {
                Picker("Distance", selection: $viewModel.distance) {
                    ForEach(DistanceCases.allCases) { distance in
                        Text(distance.localizedDescription)
                    }
                }
                Text(" ")
                Section {
                    MultiSelector(
                        label: Text("Tags"),
                        options: allTags,
                        optionToString: { $0.name },
                        selected: $viewModel.tags
                    )
                }
                Text(" ")
                Button("Clear All Filters", action: {viewModel.clearFilters()})
            }
            ZStack(alignment: .bottom) {
                Map(coordinateRegion: $viewModel.region, showsUserLocation: true, annotationItems: viewModel.filteredOpportunities) { opportunity in
                    MapAnnotation(coordinate: opportunity.coordinate) {
                        LocationAnnotation(viewModel: viewModel, opportunity: opportunity)
                    }
                }
                .padding(.bottom, 50)
                .ignoresSafeArea()
                
                NavigationLink(destination: OpportunityDetail(
                    viewModel: OpportunityDetailVM(dataStore: viewModel.dataStore, opportunity: viewModel.currentOpportunity!, locationManager: viewModel.locationManager)
                )) {
                    VStack(alignment: .leading) {
                        Text(viewModel.currentOpportunity?.title ?? "")
                            .fontWeight(.bold)
                            .foregroundColor(.black)
                        Text(viewModel.currentOpportunity?.organization ?? "")
                            .font(.caption)
                            .fontWeight(.light)
                            .foregroundColor(.black)
                        OpportunityTags(tags: viewModel.currentOpportunity?.tags ?? [])
                        .fixedSize()
                    }
                    .padding(5)
                    .background(
                        RoundedRectangle(cornerRadius: 5)
                            .stroke(Color.black, lineWidth: 1)
                    )
                    .background(
                        RoundedRectangle(cornerRadius: 5)
                            .foregroundColor(Color(white: 0.9, opacity: 0.9))
                    )
                    .opacity(viewModel.currentOpportunity == nil ? 0 : 1)
                }
                
                HStack {
                    Spacer()
                    LocationButton(.shareMyCurrentLocation){
                        viewModel.currentLocationButtonPressed()
                    }
                    .foregroundColor(.white)
                    .cornerRadius(30)
                    .labelStyle(.iconOnly)
                    .symbolVariant(.fill)
                    .frame (width: 60, height: 60)
                    .padding(.bottom, 15)
                }
                
            }
            .searchable(text: $viewModel.searchText)
            .navigationTitle("Opportunities")
        }
    }
}

struct LocationAnnotation: View {
    @ObservedObject var viewModel: MapScreenVM
    let opportunity: Opportunity
    
    var body: some View {
        VStack(spacing: 0) {
            Image(systemName: "mappin.circle.fill")
                .font(.title)
                .foregroundColor(.red)
            
            Image(systemName: "arrowtriangle.down.fill")
                .font(.caption)
                .foregroundColor(.red)
                .offset(x: 0, y: -6)
        }
        .onTapGesture {
            viewModel.opportunityTapped(opportunity: opportunity)
        }
    }
}

struct MapScreen_Previews: PreviewProvider {
    static let dataStore = DataStore()
    static let viewModel = MapScreenVM(dataStore: dataStore, locationManager: LocationManager())
    static var previews: some View {
        MapScreen(viewModel: viewModel)
    }
}


