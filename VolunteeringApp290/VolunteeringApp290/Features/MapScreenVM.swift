import Foundation
import Combine
import MapKit

class MapScreenVM: ObservableObject {
    let dataStore: DataStore
    private var cancellables: Set<AnyCancellable> = []
    let locationManager: LocationManager
    
    @Published var opportunities: [Opportunity] = []
    @Published var region = MKCoordinateRegion()
    @Published var searchText: String = ""
    @Published var distance: CLLocationDistance = -1
    @Published var tags: Set<Tag> = []
    @Published var currentOpportunity: Opportunity? = nil
    
    init(dataStore: DataStore, locationManager: LocationManager) {
        self.dataStore = dataStore
        self.locationManager = locationManager
        dataStore.$opportunities
            .sink{ [weak self] opportunitiesPublishedFromStore in
                self?.opportunities = opportunitiesPublishedFromStore
            }
            .store(in: &cancellables)
        self.region = coordinateRegion(for: opportunities.compactMap { $0.coordinate })
    }
    
    // TODO: maybe change to function and call update coordinate region?
    var filteredOpportunities: [Opportunity]  {
        return opportunities
            .filter { distance == -1 || locationManager.currentAccessibleLocation != nil && $0.distance(from: locationManager.currentAccessibleLocation!.coordinate) < distance }
            .filter { searchText.isEmpty || $0.searchableString.lowercased().contains(searchText.lowercased()) }
            .filter { tags.isEmpty || !$0.tags.intersection(tags).isEmpty }
    }
    
    func coordinateRegion(for coordinates: [CLLocationCoordinate2D]) -> MKCoordinateRegion {
        var coordinates = coordinates
        if locationManager.currentAccessibleLocation != nil {
            coordinates.append(CLLocationCoordinate2D(latitude: locationManager.currentAccessibleLocation!.coordinate.latitude, longitude: locationManager.currentAccessibleLocation!.coordinate.longitude))
        }
        guard coordinates.count > 0 else { return MKCoordinateRegion() }
        var region: MKMapRect = MKMapRect.null
        if coordinates.count == 1 {
            let point = MKMapPoint(coordinates[0])
            region = region.union(MKMapRect(x: point.x, y: point.y, width: 1000, height: 1000))
        } else {
            for coord in coordinates {
                let point = MKMapPoint(coord)
                region = region.union(MKMapRect(x: point.x, y: point.y, width: 0, height: 0))
            }
        }
        let zoomOutPercent = 1.0
        let zoomOutRegion = MKMapRect(x: region.origin.x - region.size.width*zoomOutPercent,
                                      y: region.origin.y - region.size.height*zoomOutPercent,
                                      width: region.size.width*(1+zoomOutPercent*2),
                                      height: region.size.height*(1+zoomOutPercent*2))
        return MKCoordinateRegion(zoomOutRegion)
    }
    
    func currentLocationButtonPressed() {
        locationManager.requestAllowOnceFunctionPermission()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.region = self.coordinateRegion(for: self.filteredOpportunities.compactMap { $0.coordinate })
        }
    }
    
    func opportunityTapped(opportunity: Opportunity) {
        self.currentOpportunity = opportunity
    }
    
    func clearFilters() {
        self.searchText = ""
        self.distance = -1
        self.tags = []
    }
}

