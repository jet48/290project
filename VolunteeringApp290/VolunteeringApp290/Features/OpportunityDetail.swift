import Foundation
import SwiftUI
import MapKit
import CoreLocation
import CoreLocationUI

struct OpportunityDetail: View {
    
    @StateObject var viewModel: OpportunityDetailVM
    @StateObject var locationManager = LocationManager()
    
    var body: some View {
        ScrollView {
            VStack(alignment: .leading) {
                Text(viewModel.opportunity.title)
                    .font(.largeTitle)
                    .fontWeight(.bold)
                Text(viewModel.opportunity.organization)
                    .font(.headline)
                OpportunityTags(tags: viewModel.opportunity.tags)
                Description(viewModel: viewModel)
                Spacer()
                ContactInfoDisplay(viewModel: viewModel)
                VStack(alignment: .leading) {
                    Text("Location")
                        .font(.headline)
                    Text(viewModel.opportunity.location)
                }
                .padding(6)
                Map(coordinateRegion: $viewModel.region, annotationItems: [viewModel.opportunity]) { opportunity in
                    MapAnnotation(coordinate: opportunity.coordinate) {
                        Image(systemName: "mappin.circle.fill")
                            .font(.title)
                            .foregroundColor(.red)
                        
                        Image(systemName: "arrowtriangle.down.fill")
                            .font(.caption)
                            .foregroundColor(.red)
                            .offset(x: 0, y: -6)
                    }
                }.frame(width: 300, height: 300)
                    .padding(6)
                    .toolbar {
                        ToolbarItem(placement: .primaryAction) { Button("Remove") { viewModel.removeOpportunity(opportunity: viewModel.opportunity) } }
                    }
            }
        }
        .padding(10)
    }
}

struct Description: View {
    let viewModel: OpportunityDetailVM
    
    var body: some View {
        VStack(alignment: .leading) {
            Text("Description:")
                .font(.headline)
            Text(viewModel.opportunity.description)
        }
        .padding(6)
    }
}

struct ContactInfoDisplay: View {
    let viewModel: OpportunityDetailVM
    
    var body: some View {
        VStack(alignment: .leading) {
            Text("Contact Info")
                .font(.headline)
            Text("\(viewModel.opportunity.contactInfo.email)\n\(viewModel.opportunity.contactInfo.phone)")
            Text(.init(viewModel.formattedWebsite))
        }
        .padding(6)
    }
}



struct OpportunityTags: View {
    let tags: Set<Tag>
    
    var body: some View {
        ScrollView(.horizontal) {
            HStack {
                ForEach(Array(tags), id: \.self) {
                    tag in Text(tag.name)
                        .font(.footnote)
                }
                .foregroundColor(Color.white)
                .padding(6)
                .background(RoundedRectangle(cornerRadius: 30)
                                .foregroundColor(Color.gray))
                
            }
        }
    }
}

struct OpportunityDetail_Previews: PreviewProvider {
    static let viewModel = OpportunityDetailVM(dataStore: DataStore(), opportunity: Opportunity.previewData[0], locationManager: LocationManager())
    static var previews: some View {
        OpportunityDetail(viewModel: viewModel)
    }
}
