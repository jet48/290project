import Foundation
import Combine
import MapKit

@MainActor
class OpportunityDetailVM: ObservableObject {
    let dataStore: DataStore
    var opportunity: Opportunity
    let locationManager: LocationManager
    @Published var region = MKCoordinateRegion()
    @Published var searchText: String = ""
    @Published var distance: CLLocationDistance = 10000
    @Published var tags: Set<Tag> = []
    
    private var cancellables: Set<AnyCancellable> = []

    init(dataStore: DataStore, opportunity: Opportunity, locationManager: LocationManager) {
        self.dataStore = dataStore
        self.opportunity = opportunity
        self.locationManager = locationManager
        self.region = MKCoordinateRegion(
            center:  opportunity.coordinate,
            span: MKCoordinateSpan(
              latitudeDelta: 0.01,
              longitudeDelta: 0.01
           )
        )
    }
    
    func removeOpportunity(opportunity: Opportunity) {
        dataStore.removeOpportunity(opportunity)
    }
    
    var formattedWebsite: String  {
        let website = opportunity.contactInfo.website
        if(website == nil) {
            return ""
        }
        return "[\(website!.absoluteString)](\(website!))"
    }
}
