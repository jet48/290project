import SwiftUI
import MapKit
import iPhoneNumberField
import CoreLocation

struct SheetView: View {
    @StateObject var viewModel: OpportunityListVM
    
    @Environment(\.dismiss) var dismiss
    @State private var title = ""
    @State private var organization = ""
    @State private var location = ""
    @State private var description = ""
    @State private var contactInfo = ""
    @State private var tags: Set<Tag> = []
    @State private var email = ""
    @State private var phone = ""
    @State private var website = ""
    @State private var latitude: Double = 0.0
    @State private var longitude: Double = 0.0
    

    private func isUserInformationValid() -> Bool {
        if title.isEmpty {
            return false
        }
        if organization.isEmpty {
            return false
        }
        if location.isEmpty {
            return false
        }
        if description.isEmpty {
            return false
        }
        if (description.count < 10) {
            return false
        }
        if tags.isEmpty {
            return false
        }
        if email.isEmpty {
            return false
        }
        if !(email.contains("@")) {
            return false
        }
        if !(email.contains(".")) {
            return false
        }
        if (email.count < 5) {
            return false
        }
        if phone.isEmpty {
            return false
        }
        if website.isEmpty {
            return false
        }
        if !(website.contains(".")) {
            return false
        }
        return true
    }
    
    var body: some View {
        NavigationView {
            VStack {
                Form {
                    Section(header: Text("OPPORTUNITY DETAILS")){
                        TextField("Opportunity Title", text: $title)//.textFieldStyle(.roundedBorder)
                        TextField("Organization", text: $organization)//.textFieldStyle(.roundedBorder)
                        TextField("Description (40 word limit suggested)", text: $description)//.textFieldStyle(.roundedBorder)
                    }
                    Section(header: Text("LOCATION")){
                        TextField("Address", text: $location)//.textFieldStyle(.roundedBorder)
                    }
                    Section(header: Text("CONTACT INFORMATION")){
                        TextField("Email", text: $email)//.textFieldStyle(.roundedBorder)
                        //TextField("Phone", text: $phone)//.textFieldStyle(.roundedBorder)
                        iPhoneNumberField(text: $phone)

                        TextField("Website", text: $website)//.textFieldStyle(.roundedBorder)
                    }
                    Section(header: Text("TAGS")) {
                        MultiSelector(
                            label: Text("Tags"),
                            options: allTags,
                            optionToString: { $0.name },
                            selected: $tags
                        )
                    }
                    
                }.navigationBarTitle(
                    Text("New Opportunity")
                )
                
                Button(action: {
                    viewModel.getLocation(from: location) { coordinate in
                        self.latitude = coordinate?.latitude ?? 0
                        self.longitude = coordinate?.longitude ?? 0
                        viewModel.dataStore.addOpportunity(
                            Opportunity(
                                title: title,
                                organization: organization,
                                location: location,
                                description: description,
                                contactInfo: ContactInfo(email: email, phone: phone, website: URL(string: website)),
                                tags: tags,
                                latitude: latitude,
                                longitude: longitude
                            )
                        )
                        viewModel.addSheetIsPresenting = false
                    }
                }, label: { Text("Create Opportunity") }
                ).disabled(!isUserInformationValid())
            }
            .navigationBarItems(trailing: Button("Cancel") { viewModel.doneButtonTapped() })
        }
    }
}




//struct OpportunityList_Previews: PreviewProvider {
//    static let dataStore = DataStore()
//    static let viewModel = OpportunityListVM(dataStore: dataStore, locationManager: LocationManager())
//    static var previews: some View {
//        
//        Group {
//            SheetView(viewModel: viewModel)
//            SheetView(viewModel: viewModel)
//        }
//    }
//}
//
