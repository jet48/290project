import SwiftUI
import MapKit
import CoreLocation
import CoreLocationUI

struct OpportunityList: View {
    @StateObject var viewModel: OpportunityListVM
    @EnvironmentObject var locationManager: LocationManager
    
    var body: some View {
        VStack{
            VStack {
                HStack {
                    Picker("Distance", selection: $viewModel.distance) {
                        ForEach(DistanceCases.allCases) { distance in
                            Text(distance.localizedDescription)
                        }
                    }
                    Text(" ")
                    Section {
                        MultiSelector(
                            label: Text("Tags"),
                            options: allTags,
                            optionToString: { $0.name },
                            selected: $viewModel.tags
                        )
                    }
                    Text(" ")
                    Button("Clear All Filters", action: {viewModel.clearFilters()})
                }
                List(viewModel.filteredOpportunities) { opportunity in
                    NavigationLink(destination: OpportunityDetail(
                        viewModel: OpportunityDetailVM(dataStore: viewModel.dataStore, opportunity: opportunity, locationManager: viewModel.locationManager)
                    )) {
                        OpportunityRow(opportunity: opportunity)
    
                    }
                    .swipeActions(edge: .trailing) {
                        Button(role: .destructive) {
                            viewModel.removeOpportunity(opportunity: opportunity)
                        } label: {
                            Label("Delete", systemImage: "trash")
                        }
                    }
                }
                .searchable(text: $viewModel.searchText)
                
                .navigationTitle("Opportunities")
            }.toolbar {
                ToolbarItem(placement: .primaryAction) {
                    Button("Add") { viewModel.addButtonTapped() } }
            }
            .sheet(isPresented: $viewModel.addSheetIsPresenting) {
                SheetView(viewModel: viewModel)
            }
            .onAppear { locationManager.requestAllowOnceFunctionPermission() }
        }
    }
    
    
    struct OpportunityRow: View {
        let opportunity: Opportunity
        
        var body: some View {
            VStack(alignment: .leading) {
                Text(opportunity.title)
                    .font(.headline)
                Text(opportunity.organization)
                    .font(.caption)
                OpportunityTags(tags: opportunity.tags)
            }
        }
    }
    
    
    struct OpportunityTags: View {
        let tags: Set<Tag>
        
        var body: some View {
            ScrollView(.horizontal) {
                HStack {
                    ForEach(Array(tags), id: \.self) {
                        tag in Text(tag.name)
                            .font(.footnote)
                    }
                    .foregroundColor(Color.white)
                    .padding(6)
                    .background(RoundedRectangle(cornerRadius: 30)
                                    .foregroundColor(Color.gray))
                }
            }
        }
    }
    
    struct CusttomButtonStyle: ButtonStyle {
      func makeBody(configuration: Configuration) -> some View {
        configuration.label
          .font(.caption)
          .foregroundColor(.blue)
      }
    }
    
    
    struct OpportunityList_Previews: PreviewProvider {
        static let dataStore = DataStore()
        static let viewModel = OpportunityListVM(dataStore: dataStore, locationManager: LocationManager())
        static var previews: some View {
            OpportunityList(viewModel: viewModel)
        }
    }
}
