import Foundation
import Combine
import MapKit

class OpportunityListVM: ObservableObject {
    
    let dataStore: DataStore
    let locationManager: LocationManager
    private var cancellables: Set<AnyCancellable> = []
    @Published var addSheetIsPresenting: Bool = false
    
    @Published var opportunities: [Opportunity] = []
    
    @Published var searchText: String = ""
    @Published var distance: CLLocationDistance = -1
    @Published var tags: Set<Tag> = []
    
    var currentLocation: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 36.00220133085758, longitude: -78.93112167355505)
    
    init(dataStore: DataStore, locationManager: LocationManager) {
        self.dataStore = dataStore
        self.locationManager = locationManager
        dataStore.$opportunities.sink{ [weak self] opportunitiesPublishedFromStore in
                self?.opportunities = opportunitiesPublishedFromStore
            }
        .store(in: &cancellables)
    }
    
    
    var filteredOpportunities: [Opportunity]  {
        return opportunities
            .filter { distance == -1 || locationManager.currentAccessibleLocation != nil && $0.distance(from: locationManager.currentAccessibleLocation!.coordinate) < distance }
            .filter { searchText.isEmpty || $0.searchableString.lowercased().contains(searchText.lowercased()) }
            .filter { tags.isEmpty || !$0.tags.intersection(tags).isEmpty }
    }
    
    func addButtonTapped() {
        self.addSheetIsPresenting = true
    }
    
    func removeOpportunity(opportunity: Opportunity) {
        dataStore.removeOpportunity(opportunity)
    }
    
    func doneButtonTapped() {
        addSheetIsPresenting = false
    }
    
    func getLocation(from address: String, completion: @escaping (_ location: CLLocationCoordinate2D?)-> Void) {
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(address) { (placemarks, error) in
            guard let placemarks = placemarks,
            let location = placemarks.first?.location?.coordinate else {
                completion(nil)
                return
            }
            completion(location)
        }
    }
    
    func clearFilters() {
        self.searchText = ""
        self.distance = -1
        self.tags = []
    }
    
}
