import Foundation
import MapKit

struct Opportunity: Identifiable {
    var id: UUID = UUID()
    var title: String
    var organization: String
    var location: String
    var description: String
    var contactInfo: ContactInfo
    var tags: Set<Tag> = []
    var latitude: CLLocationDegrees
    var longitude: CLLocationDegrees
    var coordinate: CLLocationCoordinate2D {
        CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
    
    func distance(from: CLLocationCoordinate2D) -> CLLocationDistance {
        return self.coordinate.distance(from: from)
    }
    
    var sortableTitle: String {
        self.title.prefix(4) == "The " ? String(self.title.dropFirst(4)) : self.title
    }
    
    var searchableString: String {
        "\(title) \(organization) \(tags.map { $0.name }.joined(separator: " "))".lowercased()
    }
}


struct ContactInfo {
    var email: String
    var phone: String
    var website: URL?
}



extension Opportunity {
    static let previewData = [
        Opportunity(title: "Soup Kitchen", organization: "Shepherds Table", location: "121 Hillsborough St #1762, Raleigh, NC 27603", description: "To provide a mutual ministry between the people of the Durham area – those who have and those who have not – by providing food obtained by donation and served by volunteers. In this manner, the true spirit of communion, that of sharing God’s gifts with God’s people, can be carried out in a caring and non-judgmental atmosphere.", contactInfo:ContactInfo(email: "SoupKitchen@shepherds-table.org", phone: "919-831-2010", website: URL(string: "http://www.shepherds-table.org")), tags: [Tag(name: "meal prep"), Tag(name: "children")], latitude: 35.780238411032, longitude: -78.6412807311667),
        Opportunity(title: "Central Park Cleanup", organization: "Durham Central Park", location: "501 Foster St, Durham, NC 27701", description: "We hold workdays year-round during the Durham Farmers’ Market to clean up the gardens and paths in the park. If you love whacking weeds, trimming brush, mulching and generally making things look spiffy around the park, from the rain garden trail to along the creek, then this is the opportunity for you! Work days are twice a month from 10am-12pm, and folks meet at the bridge on the corner of Hunt and Foster St. Our spring workdays are coming up: March 12 & 26 | April 9, 23| May 14, 28", contactInfo: ContactInfo(email: "info@durhamcentralpark.org", phone: "919-794-8194", website: URL(string: "https://durhamcentralpark.org")), tags: [Tag(name: "environment"), Tag(name: "education")], latitude: 36.00058093478167, longitude: -78.90183473122579),
        Opportunity(title: "Recess Volunteer", organization: "Playworks NC", location: "110 Corcoran Street, Durham, NC, 27701", description: "Playworks NC is a nonprofit based in Durham, NC with a presence in elementary schools within Rockingham, Wake, Durham counties. Within the schools we serve, we provide resources and training to build upon the social-emotional learning of students while incorporating conflict resolution skills through play.  We have multiple opportunities for those looking to volunteer with the youth of Durham. One can volunteer at recess with our AmeriCorp Coaches or at one of our community engagement events throughout the Spring. We would love it if any of those interested would get in contact with us to come out as be a helper on the playground with our coaches.", contactInfo: ContactInfo(email: "playworks@email.com", phone:"555-123-4567", website: URL(string: "https://www.playworks.org/north-carolina/")), tags: [Tag(name: "education"), Tag(name: "children"), Tag(name: "sports")], latitude: 35.99592858689092, longitude: -78.90128618704918),
        Opportunity(title: "Creative Reuse Volunteer", organization: "The Scrap Exchange", location: "2050 Chapel Hill Road, Durham, NC, 27707", description: "We are seeking volunteers who can commit to a regular schedule to help us with general organizing around our reuse center. Projects include things like greeting customers, sorting, packaging, merchandising, store displays and organizing materials as well as general cleaning, sweeping, and tidying.", contactInfo: ContactInfo(email: "scrapexchange@email.com", phone: "555-112-2334", website: URL(string: "http://scrapexchange.org/")), tags: [Tag(name: "environment"), Tag(name: "other")], latitude: 35.99059898024777, longitude: -78.92754513485552)
    ]
}

extension CLLocationCoordinate2D {
    /// Returns distance from coordianate in meters.
    /// - Parameter from: coordinate which will be used as end point.
    /// - Returns: Returns distance in meters.
    func distance(from: CLLocationCoordinate2D) -> CLLocationDistance {
        let from = CLLocation(latitude: from.latitude, longitude: from.longitude)
        let to = CLLocation(latitude: self.latitude, longitude: self.longitude)
        return from.distance(from: to)
    }
}


enum DistanceCases: Double, CaseIterable, Identifiable  {
    case distance = -1
    case one = 1609
    case five = 8046
    case ten = 16093
    case twenty = 32187
    var id: Double { self.rawValue }
    var localizedDescription: String {
        switch self {
        case .distance:
            return "Distance"
        case .one:
            return "1 mile"
        case .five:
            return "5 miles"
        case .ten:
            return "10 miles"
        case .twenty:
            return "20 miles"
        }
    }
}

func getTagList(String a: String) -> Set<Tag> {
    let listItems = a.components(separatedBy: ", ")
    var ret: Set<Tag> = []
    for tag in listItems {
        ret.insert((Tag(name: tag)))
    }
    return ret

}


struct Tag: Hashable, Identifiable {
    var id: String { name }
    var name: String
}

let allTags: [Tag] = [Tag(name: "education"), Tag(name: "children"), Tag(name: "elderly"), Tag(name: "environment"), Tag(name: "sports"), Tag(name: "health"), Tag(name: "research"), Tag(name: "activism"), Tag(name: "animals"), Tag(name: "fundraising/charity"), Tag(name: "meal prep"), Tag(name: "other")]
