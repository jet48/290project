import SwiftUI

enum Tab {
  case books
  case map
}

struct TabContainer: View {
  @State var selectedTab: Tab = .books
  @StateObject var dataStore = DataStore()
  @StateObject var locationManager = LocationManager()



  var body: some View {
    Group {
      TabView(selection: $selectedTab){
        NavigationView {
            OpportunityList(viewModel: OpportunityListVM(dataStore: dataStore, locationManager: locationManager))
        }
        .tabItem {
          Label("List", systemImage: "list.star")
        }
        .tag(Tab.books)

        NavigationView {
            MapScreen(viewModel: MapScreenVM(dataStore: dataStore, locationManager: locationManager))
        }
        .tabItem {
          Label("Map", systemImage: "map")
        }
        .tag(Tab.map)

      }
    }
    .environmentObject(locationManager)
  }
}

struct TabContainer_Previews: PreviewProvider {
  static var previews: some View {
    TabContainer()
  }
}
