//
//  VolunteeringApp290App.swift
//  VolunteeringApp290
//
//  Created by John Thomas on 3/17/22.
//

import SwiftUI

@main
struct VolunteeringApp290App: App {
    
    var body: some Scene {
        WindowGroup {
            TabContainer()
        }
    }
}
